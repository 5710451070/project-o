import * as React from "react"

import ScrollAnimation from "react-animate-on-scroll"
// import FadeIn from "react-fade-in"
import { Card } from "antd"
import { inject, observer } from "mobx-react"
import { Button } from "antd"
const { Meta } = Card

@observer
class Calculate extends React.Component<any, any> {
  constructor(props: any) {
    super(props)
    this.state = {
      calculate: 0,
      check: false
    }
  }

  // Hide or show the menu.

  displayData(data: any) {
    if (data >= 30) {
      return (
        <div style={{ border: "1px dotted black", padding: 10 , backgroundColor : "#ff0000"}}>
          <h6>
            {" "}
            อ้วนมาก (30.0 ขึ้นไป) ค่อนข้างอันตราย เพราะเข้าเกณฑ์อ้วนมาก
            เสี่ยงต่อการเกิดโรคร้ายแรงที่แฝงมากับความอ้วน หากค่า BMI
            อยู่ในระดับนี้ จะต้องระวังการรับประทานไขมัน
            และควรออกกำลังกายอย่างสม่ำเสมอ และหากเลขยิ่งสูงกว่า 40.0
            ยิ่งแสดงถึงความอ้วนที่มากขึ้น
          </h6>
        </div>

      )
    } else if (data >= 25 && data <= 29.9) {
      return (
        <div style={{ border: "1px dotted black", padding: 10 , backgroundColor : "#ff3300"}}>
          <h6>
            {" "}
            คุณอ้วนในระดับหนึ่ง ถึงแม้จะไม่ถึงเกณฑ์ที่ถือว่าอ้วนมาก ๆ
            แต่ก็ยังมีความเสี่ยงต่อการเกิดโรคที่มากับความอ้วนได้เช่นกัน
            ทั้งโรคเบาหวาน และความดันโลหิตสูง
          </h6>
        </div>
      )
    } else if (data >= 23 && data <= 24.9) {
      return (
        <div style={{ border: "1px dotted black", padding: 10 , backgroundColor : "#ff6600"}}>
          <h6>
            {" "}
            พยายามอีกนิดเพื่อลดน้ำหนักให้เข้าสู่ค่ามาตรฐาน เพราะค่า BMI
            ในช่วงนี้ยังถือว่าเป็นกลุ่มผู้ที่มีความอ้วนอยู่บ้าง
            แม้จะไม่ถือว่าอ้วน
            แต่หากประวัติคนในครอบครัวเคยเป็นโรคเบาหวานและความดันโลหิตสูง
            ก็ถือว่ายังมีความเสี่ยงมากกว่าคนปกติ
          </h6>
        </div>
      )
    } else if (data >= 18.6 && data <= 22.9) {
      return (
        <div style={{ border: "1px dotted black", padding: 10 , backgroundColor : "#009933"}}>
          <h6>
            {" "}
            น้ำหนักที่เหมาะสมสำหรับคนไทยคือค่า BMI ระหว่าง 18.5-22.9
            จัดอยู่ในเกณฑ์ปกติ ห่างไกลโรคที่เกิดจากความอ้วน
            และมีความเสี่ยงต่อการเกิดโรคต่าง ๆ น้อยที่สุด ควรพยายามรักษาระดับค่า
            BMI ให้อยู่ในระดับนี้ให้นานที่สุด
          </h6>
        </div>
      )
    } else {
      return (
        <div style={{ border: "1px dotted black", padding: 10 , backgroundColor : "#ffd633"}}>
          <h6>
            {" "}
            น้ำหนักน้อยกว่าปกติก็ไม่ค่อยดี หากคุณสูงมากแต่น้ำหนักน้อยเกินไป
            อาจเสี่ยงต่อการได้รับสารอาหารไม่เพียงพอหรือได้รับพลังงานไม่เพียงพอ
            ส่งผลให้ร่างกายอ่อนเพลียง่าย การรับประทานอาหารให้เพียงพอ
            และการออกกำลังกายเพื่อเสริมสร้างกล้ามเนื้อสามารถช่วยเพิ่มค่า BMI
            ให้อยู่ในเกณฑ์ปกติได้
          </h6>
        </div>
      )
    }
  }

  handleChange = ({ target }: any) => {
    this.setState({ [target.name]: target.value })
  }

  submit = (e: any) => {
    const { cm, kg } = this.state
    console.log(cm, kg)

    let parseCM = parseFloat(cm)
    let parseKG = parseFloat(kg)
    parseCM = parseCM / 100
    let total = parseKG / (parseCM * parseCM)
    console.log(total)
    this.setState({ calculate: total.toFixed(2) , check : true})
  }

  render() {
    return (
      <div className="mt-4">
        <h1>คำนวณดัชนีมวลกาย (BMI)</h1>
        <p>
          "การวัดดัชนีมวลร่างกาย" Body Mass Index (BMI) คือ
          อัตราส่วนระหว่างน้ำหนักต่อส่วนสูง ที่ใช้บ่งว่าอ้วนหรือผอม
          ในผู้ใหญ่ตั้งแต่อายุ 20 ปีขึ้นไป ความสำคัญของการรู้ค่าดัชนีมวลร่างกาย
          เพื่อดูอัตราการเสี่ยงต่อการเกิดโรคต่างๆ ถ้าค่าที่คำนวนได้
          มากหรือน้อยเกินไป เพราะถ้าเป็นโรคอ้วนแล้ว
          จะมีภาวะเสี่ยงต่อการเป็นโรคความดันโลหิตสูง โรคเบาหวาน โรคหัวใจขาดเลือด
          และโรคนิ่วในถุงน้ำดี แต่ในขณะเดียวกัน ผู้ที่ผอมเกินไป
          ก็จะเสี่ยงต่อการติดเชื้อ ประสิทธิภาพในการทำงานของร่างกายลดลง
          ดังนั้นควรรักษาระดับน้ำหนักให้อยู่ในเกณฑ์ปกติ
        </p>
        <h2>โปรแกรมคำนวณดัชนีมวลกาย Body Mass Index (BMI)</h2>
        <img className="ml-5 mb-5" src="./images/ca.jpg" />
        <br />
        <div className="row">
          <div className="col-md-6">
            <div className="row">
              <div className="col-md-2 text-right" style={{ padding: "8px" }}>
                <h6 className="">ส่วนสูง</h6>
              </div>
              <div className="col-md-7">
                {" "}
                <input
                  className="form-group form-control"
                  type="text"
                  placeholder="ส่วนสูง (cm.)"
                  name="cm"
                  value={this.state.cm}
                  onChange={this.handleChange}
                />
              </div>
              <div className="col-md-2 text-left" style={{ padding: "8px" }}>
                <h6 className="">เซนติเมตร</h6>
              </div>
            </div>
            <div className="row">
              <div className="col-md-2 text-right" style={{ padding: "8px" }}>
                <h6 className="">น้ำหนัก</h6>
              </div>
              <div className="col-md-7">
                {" "}
                <input
                  className="form-group form-control"
                  type="text"
                  placeholder="น้ำหนัก (Kg.)"
                  name="kg"
                  value={this.state.kg}
                  onChange={this.handleChange}
                />
              </div>
              <div className="col-md-2 text-left" style={{ padding: "8px" }}>
                <h6 className="">กิโลกรัม</h6>
              </div>
            </div>
            <div className="row">
              <div className="col-md-9 text-right">
                <Button onClick={this.submit} type="primary">
                  คำนวณค่า BMI
                </Button>
              </div>
            </div>
          </div>
          <div className="col-md-6">
            <img
              src="./images/1150.jpg"
              style={{ width: "100%", height: 150 }}
            />
          </div>
        </div>
        {this.state.check && (
            <div className="mt-4 row">
              <div className="col-md-4">
                <h3>ค่าที่ได้ = {this.state.calculate}</h3>
              </div>
              <div className="col-md-8">
                {this.displayData(this.state.calculate)}
              </div>
            </div>
    
        )}
      </div>
    )
  }
}

export default Calculate

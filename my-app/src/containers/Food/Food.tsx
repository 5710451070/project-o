import * as React from "react"

import { Table, Input, Button, Icon, Modal, message } from "antd"
import Highlighter from "react-highlight-words"
import axios from "axios"
import { inject, observer } from "mobx-react"
@inject("routing", "users")
@observer
class Food extends React.Component<any, any> {
  state = {
    searchText: "",
    food: [],
    visible: false,
    name: "",
    amount: 0,
    cal: ""
  }

  componentDidMount = async () => {
    let test = await axios.get(`http://localhost:3001/food`)
    console.log(test)
    this.setState({ food: test.data })
  }

  getColumnSearchProps = (dataIndex: string | number) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }: any) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: (filtered: any) => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (
      value: { toLowerCase: () => void },
      record: {
        [x: string]: {
          toString: () => {
            toLowerCase: () => { includes: (arg0: any) => void }
          }
        }
      }
    ) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible: any) => {
      if (visible) {
        setTimeout(() => this.searchInput && this.searchInput.select())
      }
    },
    render: (text: { toString: () => any }) => (
      <Highlighter
        highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text && text.toString()}
      />
    )
  })

  handleSearch = (selectedKeys: any[], confirm: () => void) => {
    confirm()
    this.setState({ searchText: selectedKeys[0] })
  }

  handleReset = (clearFilters: () => void) => {
    clearFilters()
    this.setState({ searchText: "" })
  }
  searchInput!: Input | null

  showModal = (e: any) => {
    this.setState({
      visible: true
    })
  }

  handleOk = async (e: any) => {
    const { name, amount, cal } = this.state
    if (!name || !amount || !cal) {
      return message.error("กรุณากรอกข้อมูลให้ครบ")
    }
    const result = await axios.post(`http://localhost:3001/food`, {
      name,
      amount,
      calorie: `${cal} กิโลแคลอรี่`
    })
    const result2 = await axios.get(`http://localhost:3001/food`)

    this.setState(
      {
        visible: false,
        food: result2.data,
        name: "",
        amount: "",
        cal: ""
      },
      () => message.success("บันทึกรายการอาหารสำเร็จ")
    )
  }

  handleCancel = (e: any) => {
    console.log(e)
    this.setState({
      visible: false
    })
  }

  handleChange = ({ target }: any) => {
    this.setState({ [target.name]: target.value })
  }

  render() {
    const columns: any = [
      {
        title: "ชื่อ",
        dataIndex: "name",
        key: "name",
        width: "30%",
        ...this.getColumnSearchProps("name")
      },
      {
        title: "จำนวน",
        dataIndex: "amount",
        key: "amount",
        width: "20%",
        ...this.getColumnSearchProps("amount")
      },
      {
        title: "แคลอรี่",
        dataIndex: "calorie",
        key: "calorie",
        ...this.getColumnSearchProps("calorie")
      }
    ]

    return (
      <div>
        <div className="text-center">
          <h1>อาหาร</h1>
        </div>
        {this.props.users.isLoggedIn && (
          <React.Fragment>
            <div className="d-flex">
              <Button onClick={this.showModal} type="primary">
                เพิ่มรายการอาหาร
              </Button>
            </div>
            <Table columns={columns} dataSource={this.state.food} />

            <Modal
              title="เพิ่มรายการอาหาร"
              visible={this.state.visible}
              onOk={this.handleOk}
              onCancel={this.handleCancel}
            >
              <div className="row">
                <div className="col-md-3">ชื่อ</div>
                <div className="col-md-8">
                  <input
                    className="form-control"
                    name="name"
                    onChange={this.handleChange}
                    value={this.state.name}
                  />
                </div>
              </div>
              <div className="row mt-2">
                <div className="col-md-3">จำนวน</div>
                <div className="col-md-8">
                  {" "}
                  <input
                    className="form-control"
                    name="amount"
                    onChange={this.handleChange}
                    value={this.state.amount}
                  />
                </div>
              </div>
              <div className="row mt-2">
                <div className="col-md-3">แคลอรี่</div>
                <div className="col-md-8">
                  {" "}
                  <input
                    className="form-control"
                    name="cal"
                    onChange={this.handleChange}
                    value={this.state.cal}
                  />
                </div>
              </div>
            </Modal>
          </React.Fragment>
        )}
      </div>
    )
  }
}

export default Food

import * as React from "react"

import ScrollAnimation from "react-animate-on-scroll"
// import FadeIn from "react-fade-in"
import { Card } from "antd"
import { inject, observer } from "mobx-react"
import { Button, message, Modal } from "antd"
import YouTube from "react-youtube"
import axios from "axios"

import { Tabs, Select } from "antd"

const { TabPane } = Tabs
const { Option } = Select
const { Meta } = Card

@inject("routing", "users")
@observer
class Clips extends React.Component<any, any> {
  constructor(props: any) {
    super(props)
    this.state = {
      calculate: 0,
      check: false,
      data: [],
      visible: false,
      name: "",
      sort: 0,
      link: ""
    }
  }

  // Hide or show the menu.

  async componentDidMount() {
    console.log("teeSt")
    let test = await axios.get(`http://localhost:3001/clips`)
    console.log(test)
    this.setState({ data: test.data })
  }

  _onReady(event: any) {
    // access to player in all event handlers via event.target
    event.target.pauseVideo()
  }

  displayClip = (clip: any) => {
    const opts: any = {
      height: "300",
      width: "600",
      playerVars: {
        // https://developers.google.com/youtube/player_parameters
        autoplay: 1
      }
    }
    return clip.map((val: any) => {
      return (
        <div className="my-4" key={val.clip}>
          <h1>{val.name}</h1>
          <YouTube videoId={val.clip} opts={opts} onReady={this._onReady} />
        </div>
      )
    })
  }

  showModal = (e: any) => {
    this.setState({
      visible: true
    })
  }

  handleOk = async (e: any) => {
    const { name, sort, link } = this.state

    if (!name || !sort || !link) {
      return message.error("กรุณากรอกข้อมูลให้ครบ")
    }
    let parts
    if (link) {
      parts = link.split("watch?v=")
    }
    const result = await axios.post(`http://localhost:3001/clips`, {
      name,
      sort,
      clip: parts[1]
    })
    const result2 = await axios.get(`http://localhost:3001/clips`)

    this.setState(
      {
        visible: false,
        data: result2.data,
        name: "",
        link: ""
      },
      () => message.success("บันทึกรายการอาหารสำเร็จ")
    )
  }

  handleCancel = (e: any) => {
    console.log(e)
    this.setState({
      visible: false
    })
  }
  handleChange = ({ target }: any) => {
    this.setState({ [target.name]: target.value })
  }

  handleCurrencyChange = (e: any) => {
    console.log(e)
    this.setState({ sort: e })
  }

  render() {
    const opts: any = {
      height: "300",
      width: "600",
      playerVars: {
        // https://developers.google.com/youtube/player_parameters
        autoplay: 1
      }
    }

    let { data } = this.state
    const clip1 = data.filter((val: { sort: number }): any => val.sort === 1)
    const clip2 = data.filter((val: { sort: number }): any => val.sort === 2)
    const clip3 = data.filter((val: { sort: number }): any => val.sort === 3)
    const clip4 = data.filter((val: { sort: number }): any => val.sort === 4)
    const clip5 = data.filter((val: { sort: number }): any => val.sort === 5)
    const clip6 = data.filter((val: { sort: number }): any => val.sort === 6)
    const clip7 = data.filter((val: { sort: number }): any => val.sort === 7)
    const clip8 = data.filter((val: { sort: number }): any => val.sort === 8)
    return (
      <div className="mt-4">
        {this.props.users.isLoggedIn && (
          <div className="text-right">
            <Button onClick={this.showModal} type="primary">
              เพิ่มคลิป
            </Button>
          </div>
        )}

        <Tabs tabPosition={"left"}>
          <TabPane tab="ท่าเล่นกล้ามไหล่" key="1">
            {this.displayClip(clip1)}
          </TabPane>
          <TabPane tab="ท่าเล่นกล้ามอก" key="2">
            {this.displayClip(clip2)}
          </TabPane>
          <TabPane tab="ท่าเล่นกล้ามหลัง" key="3">
            {this.displayClip(clip3)}
          </TabPane>
          <TabPane tab="ท่าเล่นกล้ามแขนด้านหน้า (ไบเซป)" key="4">
            {this.displayClip(clip4)}
          </TabPane>
          <TabPane tab="ท่าเล่นกล้ามแขนด้านหลัง" key="5">
            {this.displayClip(clip5)}
          </TabPane>
          <TabPane tab="ท่าเล่นกล้ามต้นขา" key="6">
            {this.displayClip(clip6)}
          </TabPane>
          <TabPane tab="ท่าเล่นกล้ามท้อง" key="7">
            {this.displayClip(clip7)}
          </TabPane>
          <TabPane tab="ท่าเล่นกล้ามน่อง" key="8">
            {this.displayClip(clip8)}
          </TabPane>
        </Tabs>
        <Modal
          title="เพิ่มรายการวีดีโอ"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <div className="row">
            <div className="col-md-3">ชื่อ</div>
            <div className="col-md-8">
              <input
                className="form-control"
                name="name"
                onChange={this.handleChange}
                value={this.state.name}
              />
            </div>
          </div>
          <div className="row mt-2">
            <div className="col-md-3">ประเภท</div>
            <div className="col-md-8">
              {" "}
              <Select
                style={{ width: "100%" }}
                // value={state.currency}
                // size={size}
                // style={{ width: '32%' }}
                onChange={this.handleCurrencyChange}
              >
                <Option value={1}>ท่าเล่นกล้ามไหล่</Option>
                <Option value={2}>ท่าเล่นกล้ามอก</Option>
                <Option value={3}>ท่าเล่นกล้ามหลัง</Option>
                <Option value={4}>ท่าเล่นกล้ามแขนด้านหน้า (ไบเซป)</Option>
                <Option value={5}>ท่าเล่นกล้ามแขนด้านหลัง</Option>
                <Option value={6}>ท่าเล่นกล้ามต้นขา</Option>
                <Option value={7}>ท่าเล่นกล้ามท้อง</Option>
                <Option value={8}>ท่าเล่นกล้ามน่อง</Option>
              </Select>
            </div>
          </div>
          <div className="row mt-2">
            <div className="col-md-3">url link</div>
            <div className="col-md-8">
              {" "}
              <input
                className="form-control"
                name="link"
                onChange={this.handleChange}
                value={this.state.link}
              />
            </div>
          </div>
        </Modal>
      </div>
    )
  }
}

export default Clips

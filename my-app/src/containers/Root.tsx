import * as React from "react"
import Header from "../components/HeadBar/Header"

export class Root extends React.Component<any, any> {

  render() {
    return (
      <React.Fragment>
        <Header/>
      <div className="container"> 
        {this.props.children}
        {/* {this.renderDevTool()} */}
      </div>
      </React.Fragment>
    )
  }
}

import * as React from "react"

import ScrollAnimation from "react-animate-on-scroll"
// import FadeIn from "react-fade-in"
import { Card, Radio } from "antd"
import { inject, observer } from "mobx-react"
import { Button, Select } from "antd"
const { Meta } = Card
const RadioGroup = Radio.Group
const { Option } = Select
@observer
class BMR extends React.Component<any, any> {
  constructor(props: any) {
    super(props)
    this.state = {
      value: 1
    }
  }

  // Hide or show the menu.
  onChange = (e: any) => {
    console.log("radio checked", e.target.value)
    this.setState({
      value: e.target.value
    })
  }

  handleSelect = (value: any) => {
    console.log(value)
    this.setState({ selected: value })
  }

  handleChange = ({ target }: any) => {
    this.setState({ [target.name]: target.value })
  }
  submit = () => {
    const { weight, height, age, value, selected } = this.state
    console.log(weight, height, age)

    let bmr : number = 0
    let bmrr : number = 0
    if (value === 1) {
      bmr = 66 + 13.7 * +weight + 5 * +height - 6.8 * +age
      console.log(bmr)
    } else {
      bmr = 665 + 9.6 * +weight + 1.8 * +height - 4.7 * +age
      console.log(+bmr)
    }

    if (selected === "1") {
      bmrr = bmr * 1.2
    } else if (selected === "2") {
      bmrr = bmr * 1.375
    } else if (selected === "3") {
      bmrr = bmr * 1.55
    } else if (selected === "4") {
      bmrr = bmr * 1.725
    } else if (selected === "5") {
      bmrr = bmr * 1.9
    }
    bmr = Math.ceil(bmr)
    bmrr = Math.ceil(bmrr)
    this.setState({ bmr, bmrr })
  }
  render() {
    return (
      <div className="mt-4">
        <i className="glyphicon glyphicon-heart" />
        <h1>คำนวณการเผาผลาญพลังงาน Basal Metabolic Rate (BMR) </h1>
        <p>
          Basal Metabolic Rate (BMR) คือ
          อัตราการความต้องการเผาผลาญของร่างกายในชีวิตประจำวัน
          หรือจำนวนแคลอรี่ขั้นต่ำที่ต้องการใช้ในชีวิตแต่ละวัน ดังนั้นการคำนวณ
          BMR
          จะช่วยให้คุณคำนวณปริมาณแคลอรี่ที่ใช้ต่อวันเพื่อรักษาน้ำหนักปัจจุบันได้
          และเมื่ออายุมากขึ้นเราจะควบคุมน้ำหนักได้ยากขึ้น เพราะ BMR เราลดลง
          การอดอาหารก็เป็นสาเหตุหนึ่งที่ทำให้ BMR ลดลง วิธีป้องกันคือ
          "หมั่นออกกำลังกาย" เพื่อเพิ่มประสิทธิภาพของการเผาผลาญ ซึ่งจะทำให้ BMR
          ไม่ลดลงเร็วเกินไป
        </p>
        <div className="text-center">
          <h2>คำนวณการเผาผลาญพลังงาน Basal Metabolic Rate (BMR)</h2>
          <RadioGroup onChange={this.onChange} value={this.state.value}>
            <Radio value={1}>ชาย</Radio>
            <Radio value={2}>หญิง</Radio>
          </RadioGroup>
          <div className="row mt-4">
            <div className="col-md-4 text-right">
              <span>ส่วนสูง/เซ็นติเมตร</span>
            </div>

            <div className="col-md-8 text-left">
              <input
                type="text"
                className="form-control"
                style={{ width: "50%" }}
                name="height"
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className="row mt-4">
            <div className="col-md-4 text-right">
              <span>น้ำหนัก/กิโลกรัม</span>
            </div>

            <div className="col-md-8 text-left">
              <input
                type="text"
                className="form-control"
                style={{ width: "50%" }}
                name="weight"
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className="row mt-4">
            <div className="col-md-4 text-right">
              <span>อายุ/ปี</span>
            </div>

            <div className="col-md-8 text-left">
              <input
                type="text"
                className="form-control"
                style={{ width: "50%" }}
                name="age"
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className="row mt-4">
            <div className="col-md-4 text-right">
              <span>กิจกรรม</span>
            </div>

            <div className="col-md-8 text-left">
              <Select
                // value={state.currency}
                // size={size}
                style={{ width: "50%" }}
                placeholder="กรุณาเลือกกิจกรรม"
                onChange={this.handleSelect}
              >
                <Option value="1">ไม่ออกกำลังกายหรือออกกำลังกายน้อยมาก</Option>
                <Option value="2">
                  ออกกำลังกายน้อยเล่นกีฬา 1-3 วัน/สัปดาห์
                </Option>
                <Option value="3">
                  ออกกำลังกายปานกลางเล่นกีฬา 3-5 วัน/สัปดาห์
                </Option>
                <Option value="4">
                  ออกกำลังกายหนักเล่นกีฬา 6-7 วัน/สัปดาห์
                </Option>
                <Option value="5">ออกกำลังกายหนักมากเป็นนักกีฬา</Option>
              </Select>
            </div>
          </div>
          <Button className="mt-3" type="primary" onClick={this.submit}>
            คำนวณ
          </Button>

          <div className="mt-4">
            <div className="bg py-3">
              BMR (Basal Metabolic Rate) พลังงานที่จำเป็นพื้นฐานในการมีชีวิต{" "}
              <span style={{color : "red"}}>{this.state.bmr}</span> กิโลแคลอรี่
            </div>
            <div className="mt-2 bg py-3">
              TDEE (Total Daily Energy Expenditure) พลังงานที่คุณใช้ในแต่ละวัน{" "}
              <span style={{color : "red"}}>{this.state.bmrr}</span> กิโลแคลอรี่
            </div>
          </div>

          {/* <div className="row">
            <div className="col-md-2 text-center" style={{ padding: "8px" }}>
              <h6 className="">ส่วนสูง</h6>
            </div>
            <div className="col-md-7">
              {" "}
              <input
                className="form-group form-control"
                type="text"
                placeholder="ส่วนสูง (cm.)"
                name="cm"
                value={this.state.cm}
              />
            </div>
          </div> */}
        </div>
        <h1 className="mt-5">
          วิธีคำนวณการเผาผลาญพลังงาน Basal Metabolic Rate (BMR)
        </h1>
        <h6>สูตรคำนวณอัตราการเผาผลาญของร่างกายในชีวิตประจำวันคือ</h6>
        <ul>
          <li>
            สำหรับผู้ชาย : BMR = 66 + (13.7 x น้ำหนักตัวเป็น กก.) + (5 x
            ส่วนสูงเป็น ซม.) – (6.8 x อายุ)
          </li>
          <li>
            สำหรับผู้หญิง : BMR = 665 + (9.6 x น้ำหนักตัวเป็น กก.) + (1.8 x
            ส่วนสูงเป็น ซม.) – (4.7 x อายุ)
          </li>
        </ul>
        <p>
          จะสังเกตได้ว่าน้ำหนัก ส่วนสูงและอายุมีผลต่อการเผาผลาญพลังงาน
          เมื่อหาค่า BMR (Basal Metabolic Rate)
          มาแล้วเราก็จะสามารถรู้ได้ว่าเรามีการการเผาผลาญพลังงานโดยไม่ทำกิจกรรมอะไรเลยเท่าไร
          แต่หากเรามีกิจกรรมอย่างออกกำลังกายจะมีการเผาผลาญพลังงานโดยคำนวณได้ดังนี้
        </p>
        <div className="text-center font-weight-bold">
          "การเผาผลาญพลังงานโดยปกติ = BMR x ตัวแปร"
        </div>
        <p>โดยตัวแปรของเราจะขึ้นอยู่กับการออกกำลังของเราดังนี้</p>
        <ul>
          <li>นั่งทำงานอยู่กับที่ และไม่ได้ออกกำลังกายเลย = BMR x 1.2</li>
          <li>
            ออกกำลังกายหรือเล่นกีฬาเล็กน้อย ประมาณอาทิตย์ละ 1-3 วัน = BMR x
            1.375
          </li>
          <li>
            ออกกำลังกายหรือเล่นกีฬาปานกลาง ประมาณอาทิตย์ละ 3-5 วัน = BMR x 1.55
          </li>
          <li>
            ออกกำลังกายหรือเล่นกีฬาอย่างหนัก ประมาณอาทิตย์ละ 6-7 วัน = BMR x
            1.725
          </li>
          <li>ออกกำลังกายหรือเล่นกีฬาอย่างหนักทุกวันเช้าเย็น = BMR x 1.9</li>
        </ul>
      </div>
    )
  }
}

export default BMR

import * as React from "react"
import "./index.css"

import ScrollAnimation from "react-animate-on-scroll"
import { Card } from "antd"
import { inject, observer } from "mobx-react"
import store from "store"
const { Meta } = Card

@inject("routing", "users")
@observer
class Main extends React.Component<any, any> {
  constructor(props: any) {
    super(props)
    this.state = {
      prevScrollpos: window.pageYOffset,
      visible: true,
      current: "mail"
    }
  }

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll)
  }

  // Remove the event listener when the component is unmount.
  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll)
  }

  handleClick = (e: any) => {
    console.log("click ", e)
    this.setState({
      current: e.key
    })
  }

  // Hide or show the menu.
  handleScroll = () => {
    const { prevScrollpos } = this.state

    const currentScrollPos = window.pageYOffset
    const visible = prevScrollpos > currentScrollPos

    this.setState({
      prevScrollpos: currentScrollPos,
      visible
    })
  }
  render() {
    return (
      <div className="text-center mt-4">
        <img src="./images/1.jpg" style={{ height: 100 }} />
        <div className="mt-4">Fitness And Food For Health</div>
        <div className="d-flex justify-content-between mt-5">
          <ScrollAnimation animateIn="bounceInLeft" delay={100}>
            <Card
              onClick={() => this.props.routing.push("/loseweight")}
              hoverable
              style={{ width: 240, height: 300, backgroundColor: "darksalmon" }}
              cover={<img alt="example" src="./images/386.jpg" />}
            >
              <Meta title="ลดน้ำหนัก" />
            </Card>
          </ScrollAnimation>

          <ScrollAnimation animateIn="bounceInLeft" delay={500}>
            <Card
              onClick={() => this.props.routing.push("/muscle")}
              hoverable
              style={{ width: 240, height: 300, backgroundColor: "darksalmon" }}
              cover={<img alt="example" src="./images/387.jpg" />}
            >
              <Meta title="เพิ่มกล้ามเนื้อ" />
            </Card>
          </ScrollAnimation>

          <ScrollAnimation animateIn="bounceInLeft" delay={1000}>
            <Card
              onClick={() => this.props.routing.push("/strong")}
              hoverable
              style={{ width: 240, height: 300, backgroundColor: "darksalmon" }}
              cover={<img alt="example" src="./images/388.jpg" />}
            >
              <Meta title="เพิ่มความแข็งแรง" />
            </Card>
          </ScrollAnimation>
        </div>
      </div>
    )
  }
}

export default Main

import * as React from "react"
import logo from "./logo.svg"
import "./App.css"
import { hot } from "react-hot-loader"
import { Root } from "./containers/Root"
import { Route, Router, Switch } from "react-router"
import Main from "./containers/Main/"
import Calculate from "./containers/ฺBMI/Calculate"
import BMR from "./containers/BMR/BMR"
import One from "./containers/Main/One"
import Two from "./containers/Main/Two"
import Three from "./containers/Main/Three"
import Clips from "./containers/Clips/Clips"
import Food from "./containers/Food/Food";
import Login from "./components/Login/Login";

export const App = hot(module)(({ history }: any) => (
  <Root>
    <Router history={history}>
      <Switch >
          <Route exact path="/" component={Main}/>
          <Route exact path="/BMI" component={Calculate}/>
          <Route exact path="/BMR" component={BMR}/>
          <Route exact path="/loseweight" component={One}/>
          <Route exact path="/muscle" component={Two}/>
          <Route exact path="/strong" component={Three}/>
          <Route exact path="/clips" component={Clips}/>
          <Route exact path="/food" component={Food}/>
          <Route exact path="/login" component={Login}/>
      </Switch>
    </Router>
  </Root>
))

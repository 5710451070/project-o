import createBrowserHistory from 'history/createBrowserHistory';
import { RouterStore, syncHistoryWithStore } from "mobx-react-router"
import userStore from "./user"

const browserHistory = createBrowserHistory()
const routerStore = new RouterStore()

export const history = syncHistoryWithStore(browserHistory, routerStore)
export const stores = {
    routing: routerStore,
    users : userStore
  }
import { observable, action, computed } from "mobx"
import store from "store"

class Users {
  @observable user: any
  @observable isFetching: any
  @observable token: any
  @observable store: any
  @observable length = 2

  initialState = {
    id: undefined,
    password: undefined
  }

  constructor() {
    this.store = undefined
    this.user = undefined
    this.isFetching = false
    this.getUser()
  }

  @action
  clearUser = () => {
    store.clearAll()
    this.user = null
  }

  @computed
  get isLoggedIn() {
    return this.user
  }
  @computed get squared() {
    return this.length * this.length
  }
  set squared(value) {
    //this is automatically an action, no annotation necessary
    console.log("TESTTTTTTTTTT")
    this.length = Math.sqrt(value) * 22
  }

  @action
  getUser() {
    this.user = store.get("user")
  }

  @action
  setUser = (user: any) => {
    this.user = user
    this.isFetching = false
  }

  @action
  logingin = () => {
    this.isFetching = true
  }

  @action
  resetUser = () => {
    this.user = this.initialState
  }
}

const users = new Users()
export default users

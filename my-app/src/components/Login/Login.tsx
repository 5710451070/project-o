import * as React from "react"

import classnames from "classnames"
import { Menu, Icon } from "antd"
import { inject, observer } from "mobx-react"
import { Link } from "react-router-dom"
import { Formik } from "formik"
import { Button } from "antd"
import axios from "axios"
import store from "store"

const SubMenu = Menu.SubMenu
const MenuItemGroup = Menu.ItemGroup

@inject("routing" , "users")
@observer
class Login extends React.Component<any, any> {
  constructor(props: any) {
    super(props)
    this.state = {
      prevScrollpos: window.pageYOffset,
      visible: true,
      current: "mail",
      user : {}
    }
  }

  async componentDidMount() {
    const result = await axios("http://localhost:3001/user")
    console.log(result)
    this.setState({user : result.data[0]})
    window.addEventListener("scroll", this.handleScroll)
  }

  // Remove the event listener when the component is unmount.
  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll)
  }

  handleClick = (e: any) => {
    console.log("click ", e)
    // if (e.key === "app") {
    //     this.props.routing.history.push("/BMI")
    // }
    this.setState({
      current: e.key
    })
  }

  // Hide or show the menu.
  handleScroll = () => {
    const { prevScrollpos } = this.state

    const currentScrollPos = window.pageYOffset
    const visible = prevScrollpos > currentScrollPos

    this.setState({
      prevScrollpos: currentScrollPos,
      visible
    })
  }

  onSubmit = async (e : any) => {
    console.log(e)

    // console.log(setSubmitting)
    // console.log(errors)
    // test.setSubmitting(false)
    // setTimeout(() => {
    //   alert(JSON.stringify(values, null, 2))
    
    // }, 400)
  }
  render() {
    const { location, push, goBack } = this.props.routing
    return (
      <div
        className="d-flex justify-content-center align-items-center flex-column"
        style={{ height: "400px" }}
      >
        <div>
          <h1>เข้าสู่ระบบ</h1>
        </div>
        <Formik
          initialValues={{ email: "", password: "" }}
          validate={values => {
            let errors: any = {}
            return errors
          }}
          onSubmit={(values , dd) => {
            let errors: any = {}
            if (values.email === "flouk1150@gmail.com") {
              errors.password = "dddddddddddddddddddddddddddddddddd"
            }
           

            const { user } = this.state
            console.log(values)
            console.log(user)
            if (values.email !== user.user && values.password !== user.password) {
              dd.setFieldError("password" , "กรุณากรอกรหัสให้ถูกต้อง")
            }
            else {
              store.set("user", values)
              this.props.users.getUser()
              push("/")
            }
        
            dd.setSubmitting(false)
            // dd.validateForm(values)

            
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting
            /* and other goodies */
          }) => (
            <React.Fragment>
              <form onSubmit={(e) => handleSubmit(e)} style={{ width: "300px" }}>
                <div>
                  <input
                    className="form-control"
                    placeholder="id"
                    type="text"
                    name="email"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.email}
                  />
                </div>
                <div>
                  {errors.email && touched.email && errors.email}
                  <input
                    className="form-control mt-3"
                    placeholder="password"
                    type="password"
                    name="password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.password}
                  />
                     {errors.password && errors.password}
                  <div className="text-center">
                    <Button
                      htmlType="submit"
                      type="primary"
                      className="mt-3 btn-block"
                      disabled={isSubmitting}
                    >
                      เข้าสู่ระบบ
                    </Button>
                  </div>
                </div>
              </form>
            </React.Fragment>
          )}
        </Formik>
      </div>
    )
  }
}

export default Login

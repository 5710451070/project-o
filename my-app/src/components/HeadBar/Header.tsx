import * as React from "react"

import classnames from "classnames"
import { Menu, Icon } from "antd"
import "./Header.css"
import { inject, observer } from "mobx-react"
import { Link } from "react-router-dom"

const SubMenu = Menu.SubMenu
const MenuItemGroup = Menu.ItemGroup

@inject("routing", "users")
@observer
class Header extends React.Component<any, any> {
  constructor(props: any) {
    super(props)
    this.state = {
      prevScrollpos: window.pageYOffset,
      visible: true,
      current: "mail"
    }
  }

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll)
  }

  // Remove the event listener when the component is unmount.
  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll)
  }

  handleClick = (e: any) => {
    console.log("click ", e)
    // if (e.key === "app") {
    //     this.props.routing.history.push("/BMI")
    // }
    this.setState({
      current: e.key
    })
  }

  // Hide or show the menu.
  handleScroll = () => {
    const { prevScrollpos } = this.state

    const currentScrollPos = window.pageYOffset
    const visible = prevScrollpos > currentScrollPos

    this.setState({
      prevScrollpos: currentScrollPos,
      visible
    })
  }

  logout = () => {
    const { location, push, goBack } = this.props.routing
    this.props.users.clearUser()
    push("/login")
  }
  render() {
    const { location, push, goBack } = this.props.routing
    return (
      <div>
        <Menu
          className={classnames("navbarr", {
            "navbar--hidden": !this.state.visible
          })}
          onClick={this.handleClick}
          selectedKeys={[this.state.current]}
          mode="horizontal"
        >
          <Menu.Item key="mail" onClick={() => push("/")}>
            <Icon type="mail" />
            หน้าหลัก
          </Menu.Item>
          <Menu.Item key="app" onClick={() => push("/BMI")}>
            <Icon type="appstore" />
            คำนวณ BMI
          </Menu.Item>

          <Menu.Item key="alipay" onClick={() => push("/BMR")}>
            คำนวณ พลังงาน
          </Menu.Item>
          <Menu.Item key="a" onClick={() => push("/food")}>
            <Icon type="mail" />
            Calories food
          </Menu.Item>
          <Menu.Item key="b" onClick={() => push("/clips")}>
            <Icon type="appstore" />
            ท่าออกกำลังกาย
          </Menu.Item>

          {/* <Menu.Item key="c">เว็บบอร์ด</Menu.Item> */}

          {!this.props.users.isLoggedIn ? (
            <Menu.Item
              className="ml-auto"
              key="cc"
              onClick={() => push("/login")}
            >
              เข้าสู่ระบบแอดมิน
            </Menu.Item>
          ) : (
            <Menu.Item
              className="ml-auto"
              key="cc"
              onClick={this.logout}
            >
              ออกจากระบบแอดมิน
            </Menu.Item>
          )}
        </Menu>
      </div>
    )
  }
}

export default Header
